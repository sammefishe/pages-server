package upstream

import (
	"strconv"
	"strings"
	"time"

	"codeberg.org/codeberg/pages/server/cache"
	"codeberg.org/codeberg/pages/server/context"
	"codeberg.org/codeberg/pages/server/gitea"
	"github.com/rs/zerolog/log"
)

type Redirect struct {
	From       string
	To         string
	StatusCode int
}

// rewriteURL returns the destination URL and true if r matches reqURL.
func (r *Redirect) rewriteURL(reqURL string) (dstURL string, ok bool) {
	// check if from url matches request url
	if strings.TrimSuffix(r.From, "/") == strings.TrimSuffix(reqURL, "/") {
		return r.To, true
	}
	// handle wildcard redirects
	if strings.HasSuffix(r.From, "/*") {
		trimmedFromURL := strings.TrimSuffix(r.From, "/*")
		if reqURL == trimmedFromURL || strings.HasPrefix(reqURL, trimmedFromURL+"/") {
			if strings.Contains(r.To, ":splat") {
				matched := strings.TrimPrefix(reqURL, trimmedFromURL)
				matched = strings.TrimPrefix(matched, "/")
				return strings.ReplaceAll(r.To, ":splat", matched), true
			}
			return r.To, true
		}
	}
	return "", false
}

// redirectsCacheTimeout specifies the timeout for the redirects cache.
var redirectsCacheTimeout = 10 * time.Minute

const redirectsConfig = "_redirects"

// getRedirects returns redirects specified in the _redirects file.
func (o *Options) getRedirects(ctx *context.Context, giteaClient *gitea.Client, redirectsCache cache.ICache) []Redirect {
	var redirects []Redirect
	cacheKey := o.TargetOwner + "/" + o.TargetRepo + "/" + o.TargetBranch

	// Check for cached redirects
	if cachedValue, ok := redirectsCache.Get(cacheKey); ok {
		redirects = cachedValue.([]Redirect)
	} else {
		// Get _redirects file and parse
		body, err := giteaClient.GiteaRawContent(ctx, o.TargetOwner, o.TargetRepo, o.TargetBranch, redirectsConfig)
		if err == nil {
			for _, line := range strings.Split(string(body), "\n") {
				redirectArr := strings.Fields(line)

				// Ignore comments and invalid lines
				if strings.HasPrefix(line, "#") || len(redirectArr) < 2 {
					continue
				}

				// Get redirect status code
				statusCode := 301
				if len(redirectArr) == 3 {
					statusCode, err = strconv.Atoi(redirectArr[2])
					if err != nil {
						log.Info().Err(err).Msgf("could not read %s of %s/%s", redirectsConfig, o.TargetOwner, o.TargetRepo)
					}
				}

				redirects = append(redirects, Redirect{
					From:       redirectArr[0],
					To:         redirectArr[1],
					StatusCode: statusCode,
				})
			}
		}
		_ = redirectsCache.Set(cacheKey, redirects, redirectsCacheTimeout)
	}
	return redirects
}

func (o *Options) matchRedirects(ctx *context.Context, giteaClient *gitea.Client, redirects []Redirect, redirectsCache cache.ICache) (final bool) {
	reqURL := ctx.Req.RequestURI
	// remove repo and branch from request url
	reqURL = strings.TrimPrefix(reqURL, "/"+o.TargetRepo)
	reqURL = strings.TrimPrefix(reqURL, "/@"+o.TargetBranch)

	for _, redirect := range redirects {
		if dstURL, ok := redirect.rewriteURL(reqURL); ok {
			if o.TargetPath == dstURL { // recursion base case, rewrite directly when paths are the same
				return true
			} else if redirect.StatusCode == 200 { // do rewrite if status code is 200
				o.TargetPath = dstURL
				o.Upstream(ctx, giteaClient, redirectsCache)
			} else {
				ctx.Redirect(dstURL, redirect.StatusCode)
			}
			return true
		}
	}

	return false
}
